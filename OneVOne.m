clear all 
clc
X=importdata('dnaTrainX');
Y=importdata('dnaTrainY');
testX=importdata('dnaTestX');
testY=importdata('dnaTestY');
X=X.data;%(1:500,:);
Y=Y.data;%(1:500,:);
TestSet=testX.data;%(1:100,:);
testY=testY.data;%(1:100,:);
u=unique(Y);
[n d] = size(X);
numClasses=length(u);
result = zeros(length(TestSet(:,1)),1);
% This is the cost of slack for SVM.
C = 4;  %tuned using cross validation but 2 usually works fine(from experience).
W = zeros(numClasses*(numClasses-1)/2,d);    % Weight vectors
b = zeros(numClasses*(numClasses-1)/2,1); % bias
model =1;
class=zeros(numClasses*(numClasses-1)/2,2);
t = cputime;
for firstclass = 1:(numClasses-1)
    for secclass = (firstclass+1):numClasses
        Xt=[];
        Yt=[];
        class(model,1)=firstclass;
        class(model,2)=secclass;
            for i = 1:n
                if (Y(i,1)==firstclass)
                    Xt = [Xt;X(i,:)];
                    Yt = [Yt;1];
                end
                if (Y(i,1)==secclass)
                    Xt = [Xt;X(i,:)];
                    Yt = [Yt;-1];
                end
            end
            l = size(Xt,1);
            K = zeros(l,l);  % Kernel matrix
            %K = linearkernel(Xt);   % uncomment one of the kernels
            %K = polykernel(Xt);     % degree 2
            K = radialbasiskernel(Xt); % with gamma = 0.5
            cvx_begin
            variables alphatrain(l);
            minimize (0.5.*quad_form(Yt.*alphatrain,K) - sum(alphatrain));
            subject to 
                alphatrain >= 0;
                alphatrain <= C;
                Yt'*alphatrain == 0; 
            cvx_end
            wtrain = 0;
    for i=1:l
        wtrain = wtrain + alphatrain(i)*Yt(i)*Xt(i,:);
    end;
    W(model,:) = wtrain;    % recording model parameters
    upper = -inf;
    lower = inf;
    for i=1:l
        if Yt(i)==-1
            upper = max(upper,W(model,:)*X(i,:)');
        else
            lower = min(lower,W(model,:)*X(i,:)');
        end
    end
    b(model,1) = -(upper+lower)/2; 
    model = model + 1;
    end
end
%classify test cases
for j=1:size(TestSet,1)
        temp = zeros(numClasses,1);
        for k = 1:numClasses*(numClasses-1)/2
                if(W(k,:)*TestSet(j,:)' + b(k,1))>0 
                    temp(class(k,1),1) = temp(class(k,1),1)+1;
                else
                    temp(class(k,2),1) = temp(class(k,2),1)+1;
                end
        end
        [M I] = max(temp(:,1));
        result(j,1) = I;
end
cputime-t
acc = sum(result==testY)/length(result(:,1))