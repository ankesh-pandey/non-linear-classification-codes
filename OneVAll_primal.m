clear all
clc
% data processing
X  = importdata('uspsTrainX');Y=importdata('uspsTrainY');testX=importdata('uspsTestX');testY=importdata('uspsTestY');
%[N D] = size(data);
%data = data(randperm(N),:);
t = cputime;
%X = data(1:floor(0.7*N),1:57); Y = data(1:floor(0.7*N),58); TestSet = data(floor(0.7*N)+1:N,1:57);
X=X.data;Y=Y.data;TestSet=testX.data;testY=testY.data;
u=unique(Y);
[n d] = size(X);
numClasses=length(u);
result = zeros(length(TestSet(:,1)),1);
% This is the cost of slack for SVM.
C = 100;  %tuned using cross validation but 2 usually works fine(from experience).
W = zeros(numClasses,d);
b = zeros(numClasses,1);

for k=1:numClasses
    G1vAll= zeros(n,1);
    for i=1:n  %picking one of the classes in each loop
         if Y(i)== u(k)
           G1vAll(i) = 1;
         else
             G1vAll(i) = -1;
        end
    end
    Yt=G1vAll(1:n,1); 
    Xt=X(1:n,:); %X is the same and Y is different
    cvx_begin %classical svm
        variables wtrain(d) e(n) btrain
        dual variable alphatrain
        minimize( 0.5*wtrain'*wtrain + C*sum(e)) 
        subject to
            Yt.*(Xt*wtrain+btrain)-1 +e >=0   :alphatrain;
            e>=0; %slack
    cvx_end
    W(k,:) = wtrain;    % recording model parameters
    b(k,1) = btrain; 
end;
%classify test cases
for j=1:size(TestSet,1)
    for k=1:numClasses
        if(W(k,:)*TestSet(j,:)' + b(k,1))>0 
            break;
        end
    end
    result(j) = k;
end
cputime - t
acc = sum(result==testY)/length(result(:,1))