function [kernel] = linearkernel(X)
    n = size(X,1);
    kernel = zeros(n,n);
    for i=1:n
        for j=1:n
            kernel(i,j) = X(i,:)*X(j,:)';
        end
    end
end