function [kernel] = radialbasiskernel(X)
    g = 1/size(X,2);
    n = size(X,1);
    kernel = zeros(n,n);
    for i=1:n
        for j=1:n
            kernel(i,j) = exp(-g*(norm(X(i,:)-X(j,:),2)^2));
        end
    end
end