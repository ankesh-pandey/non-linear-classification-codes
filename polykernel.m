function [kernel] = polykernel(X)
    c = 0;%default
    g = 1/size(X,2);
    n = size(X,1);
    kernel = zeros(n,n);
    for i=1:n
        for j=1:n
            kernel(i,j) = (c + g*X(i,:)*X(j,:)').^3;
        end
    end
end