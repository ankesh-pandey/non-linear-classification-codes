clear all
clc
% data processing
X  = importdata('uspsTrainX');Y=importdata('uspsTrainY');testX=importdata('uspsTestX');testY=importdata('uspsTestY');
%[N D] = size(data);
%data = data(randperm(N),:);
t = cputime;
%X = data(1:floor(0.7*N),1:57); Y = data(1:floor(0.7*N),58); TestSet = data(floor(0.7*N)+1:N,1:57);
X=X.data(1:500,:);Y=Y.data(1:500,:);TestSet=testX.data(1:100,:);testY=testY.data(1:100,:);
[n d] = size(X);
u=unique(Y);
numClasses=length(u);
result = zeros(length(TestSet(:,1)),1);
% This is the cost of slack for SVM.
C = 4;  %tuned using cross validation but 2 usually works fine(from experience).
W = zeros(numClasses,d);    % Weight vectors
b = zeros(numClasses,1); % bias
K = zeros(n,n);  % Kernel matrix
%K = linearkernel(X);   % uncomment one of the kernels
K = polykernel(X);     % degree 2
%K = radialbasiskernel(X); % with gamma = 0.5
for k=1:numClasses
    G1vAll= zeros(n,1);
    for i=1:n  %picking one of the classes in each loop
         if Y(i)== u(k)
           G1vAll(i) = 1;
         else
             G1vAll(i) = -1;
        end
    end
    Yt=G1vAll(1:n,1); 
    Xt=X(1:n,:); %X is the same and Y is different
    cvx_begin
        variables alphatrain(n);
        minimize (0.5.*quad_form(Yt.*alphatrain,K) - sum(alphatrain));
        subject to 
            alphatrain >= 0;
            alphatrain <= C;
            Yt'*alphatrain == 0; 
    cvx_end
    wtrain = 0;
    for i=1:n
        wtrain = wtrain + alphatrain(i)*Yt(i)*Xt(i,:);
    end;
    W(k,:) = wtrain;    % recording model parameters
    upper = -inf;
    lower = inf;
    for i=1:n
        if Yt(i)==-1
            upper = max(upper,W(k,:)*X(i,:)');
        else
            lower = min(lower,W(k,:)*X(i,:)');
        end
    end
    b(k,1) = -(upper+lower)/2; 
end;
%classify test cases
for j=1:size(TestSet,1)
    for k=1:numClasses
        if(W(k,:)*TestSet(j,:)' + b(k,1))>0 
            break;
        end
    end
    result(j) = k;
end
cputime - t
acc = sum(result==testY)/length(result(:,1))